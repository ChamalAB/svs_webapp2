# standard


# third-party
import mysql.connector


# load configs
from config import (DB_CREDS)



class databaseConnection:
    def __init__(self, **kwargs):
        self.db = mysql.connector.connect(**kwargs)
    
    
    def execute(self,sql,args,fetch=True):
        result = None
        with self.db.cursor(dictionary=True,buffered=True) as cursor:
            cursor.execute(sql,args)
            self.db.commit()
            if fetch:
                result = cursor.fetchall()
        return result
    
    
    def close(self):
        self.db.close()
        
        
    def __del__(self):
        self.close()


database = databaseConnection(**DB_CREDS)