# standard
import datetime

# third-party
from flask import redirect, Blueprint, render_template, request, abort, url_for, jsonify


# user defined
from database import database


# for stats chart
class colorCarousel:
    def __init__(self):
        self.colors = ['#FF0000','#FFFF00','#0000FF','#7FFF00','#FF8C00']
        self.count = 0
        
    def get(self):
        try:
            picked = self.colors[self.count]
        except IndexError:
            self.count = 0
            picked = self.colors[self.count]
        finally:
            self.count += 1
            
        return picked

def unixToTimestr(unixTime):
    value = datetime.datetime.fromtimestamp(unixTime)
    return f"{value:%Y-%m-%d %H:%M:%S}"


stats_blueprint = Blueprint('stats', __name__, template_folder='templates')


@stats_blueprint.route('/stats',methods=['GET', 'POST'])
def stats_view():
    if request.method == 'GET':
        return render_template('stats.html')
    else:
        data = database.execute('select * from remain',())
        dataset = {}
        dataset['table'] = data

        colors = colorCarousel()
        new_set = {}
        for item in data:
            print(item)
            if not item['cameraid'] in new_set:
                new_set[item['cameraid']] = {'label' : item['cameraid'],
                                            'borderColor': colors.get(),
                                            'data' : []}
                
            # add data
            new_set[item['cameraid']]['data'].append({'x': unixToTimestr(item['time']), 'y' : item['remain']})

        dataset['chart'] = list(new_set.values())

        return jsonify(dataset)


