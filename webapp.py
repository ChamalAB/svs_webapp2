# standard
import datetime


# third-prty
from flask import Flask, session, redirect, render_template, url_for, abort


# user defined
from live import live_blueprint
from movement import movement_blueprint
from stats import stats_blueprint

app = Flask(__name__)
app.register_blueprint(live_blueprint)
app.register_blueprint(movement_blueprint)
app.register_blueprint(stats_blueprint)


# inject time
time_now = lambda : str(datetime.datetime.now())[:19]

@app.context_processor
def inject_today_date():
    return {'today_date': time_now()}


@app.route('/')
def index():
	return render_template('home.html')


if __name__ == '__main__':
	app.run(debug=True)