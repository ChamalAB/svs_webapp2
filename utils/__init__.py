import cv2
import numpy as np

def createBlankImage(height,width,color=(0,0,0)):
    """
    Creates a blank image
    color only supports 3 channels
    Input will have to adapt to either
    RGB - Pillow
    BGR - OpenCv
    """
    # create empty array
    data = np.zeros((height,width,3),dtype=np.uint8)
    data[:] = tuple(color)
    return data