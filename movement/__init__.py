# standard


# third-party
from flask import redirect, Blueprint, render_template, request, abort, url_for, jsonify


# user defined
from database import database


movement_blueprint = Blueprint('movement', __name__, template_folder='templates')


@movement_blueprint.route('/movements',methods=['GET', 'POST'])
def movements():
	if request.method == 'GET':
		return render_template('movement.html')
	else:
		return jsonify(database.execute('select * from movement',()))