# standard
import threading

# third-party
import cv2
import imagezmq

# user defined
from utils import createBlankImage

# load  configs
from config import (enable_imageZMQ,imageZMQ_connect_to)



# a dummy output frame is placed here
outputFrame = createBlankImage(400,400, color=(128,128,128))



def update_frame(frame):
    global outputFrame
    outputFrame = frame


def generate():
    global outputFrame

    while True:
        # make sure a valid frame exist
        if outputFrame is None:
            continue

        (flag, encodedImage) = cv2.imencode(".jpg", outputFrame)

        # make sure encoding is complete
        if not flag:
            continue

        yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
            bytearray(encodedImage) + b'\r\n')


def start_capture():
	image_hub = imagezmq.ImageHub(open_port=imageZMQ_connect_to, REQ_REP=False)

	while True:  # show streamed images until Ctrl-C
	    rpi_name, image = image_hub.recv_image()
	    update_frame(image)

if enable_imageZMQ:
    t = threading.Thread(target=start_capture)
    t.setDaemon(True)
    t.start()