from flask import redirect, Blueprint, render_template, request, abort, url_for, Response

# user difened
from .models import generate


live_blueprint = Blueprint('live', __name__, template_folder='templates')



@live_blueprint.route('/liveview')
def livepage():
	return render_template('live.html')


@live_blueprint.route("/video_feed")
def video_feed():
    # return the response generated along with the specific media
    # type (mime type)
    return Response(generate(),
        mimetype = "multipart/x-mixed-replace; boundary=frame")